import { useEffect, useState } from 'react';
import yelp from '../api/yelp';

export default (id) => {
    const [business, setBusiness] = useState(null);
    const [errorMessage, setErrorMessage] = useState('');
    
    const getBusiness = async id => {
        try {
          const response = await yelp.get(`/${id}`);
          setBusiness(response.data);
        } catch (err) {
          setErrorMessage('No restaurant found.');
        }
      };
      
      useEffect(() => {
        getBusiness(id);
      }, []);
      
    return [getBusiness, business, errorMessage];
  };
  