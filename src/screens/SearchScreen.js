import React, { useState } from 'react';
import { Text, StyleSheet, View, ScrollView } from 'react-native';
import SearchBar from '../components/SearchBar';
import useBusinesses from '../hooks/useBusinesses';
import BusinessList from '../components/BusinessesList';

const SearchScreen = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchApi, businesses, errorMessage] = useBusinesses();

  const filterBusinessesByPrice = price => {
    return businesses.filter(business => {
      return business.price === price;
    });
  };

  return (
    <>
      <SearchBar
        searchTerm={searchTerm}
        onSearchTermChange={setSearchTerm}
        onSearchTermSubmit={() => searchApi(searchTerm)}
      />
      {errorMessage ? <Text>{errorMessage}</Text> : null}
      <ScrollView>
        <BusinessList
          businesses={filterBusinessesByPrice('$')}
          title='Cost Effective'
        />
        <BusinessList
          businesses={filterBusinessesByPrice('$$')}
          title='Bit Pricier'
        />
        <BusinessList
          businesses={filterBusinessesByPrice('$$$')}
          title='Big Spender'
        />
         <BusinessList
          businesses={filterBusinessesByPrice('$$$$')}
          title='Exclusive'
        />
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({});

export default SearchScreen;
