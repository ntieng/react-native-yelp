import React, { useEffect, useState } from 'react';
import {
  Text,
  StyleSheet,
  View,
  FlatList,
  Image,
  TouchableOpacity
} from 'react-native';
import yelp from '../api/yelp';
import { showLocation } from 'react-native-map-link';
import useBusinessDetails from '../hooks/useBusinessDetails';

const BusinessesShowScreen = ({ navigation }) => {
  //const [business, setBusiness] = useState(null);
  //const [errorMessage, setErrorMessage] = useState('');

  const id = navigation.getParam('id');

  const [getBusiness, business, errorMessage] = useBusinessDetails(id);

  if (!business) {
    return null;
  }

  let operatingHours = '';
  return (
    <View>
      {business.hours && business.hours.length > 0 ? (business.hours[0].open.map(item => {
        switch (item.day) {
          case 0:
            const mon =
              'Mon: ' +
              item.start.substring(0, 2) +
              ':' +
              item.start.substring(2, 4) +
              '-' +
              item.end.substring(0, 2) +
              ':' +
              item.end.substring(2, 4) +
              '\n';
            operatingHours += mon;
            break;
          case 1:
            const tue =
              'Tue: ' +
              item.start.substring(0, 2) +
              ':' +
              item.start.substring(2, 4) +
              '-' +
              item.end.substring(0, 2) +
              ':' +
              item.end.substring(2, 4) +
              '\n';
            operatingHours += tue;
            break;
          case 2:
            const wed =
              'Wed: ' +
              item.start.substring(0, 2) +
              ':' +
              item.start.substring(2, 4) +
              '-' +
              item.end.substring(0, 2) +
              ':' +
              item.end.substring(2, 4) +
              '\n';
            operatingHours += wed;
            break;
          case 3:
            const thu =
              'Thu: ' +
              item.start.substring(0, 2) +
              ':' +
              item.start.substring(2, 4) +
              '-' +
              item.end.substring(0, 2) +
              ':' +
              item.end.substring(2, 4) +
              '\n';
            operatingHours += thu;
            break;
          case 4:
            const fri =
              'Fri: ' +
              item.start.substring(0, 2) +
              ':' +
              item.start.substring(2, 4) +
              '-' +
              item.end.substring(0, 2) +
              ':' +
              item.end.substring(2, 4) +
              '\n';
            operatingHours += fri;
            break;
          case 5:
            const sat =
              'Sat: ' +
              item.start.substring(0, 2) +
              ':' +
              item.start.substring(2, 4) +
              '-' +
              item.end.substring(0, 2) +
              ':' +
              item.end.substring(2, 4) +
              '\n';
            operatingHours += sat;
            break;
          case 6:
            const sun =
              'Sun: ' +
              item.start.substring(0, 2) +
              ':' +
              item.start.substring(2, 4) +
              '-' +
              item.end.substring(0, 2) +
              ':' +
              item.end.substring(2, 4) +
              '\n';
            operatingHours += sun;
            break;
          default:
            null;
        }
      })) : null}
      {business.name ? (
        <Text style={styles.titleStyle}>{business.name}</Text>
      ) : null}
      {errorMessage ? <Text>{errorMessage}</Text> : null}

      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={business.photos}
        keyExtractor={photo => photo}
        renderItem={({ item }) => {
          return <Image style={styles.imageStyle} source={{ uri: item }} />;
        }}
      />
      <TouchableOpacity
        style={styles.buttonStyle}
        onPress={() => {
          showLocation({
            latitude: business.coordinates.latitude,
            longitude: business.coordinates.longitude
          });
        }}
      >
        <Text> Get Directions </Text>
      </TouchableOpacity>
      <Text style={styles.hoursStyle}>{operatingHours}</Text>
      {business.is_closed ? (
        <Text style={styles.textStyle}>Open Now: No</Text>
      ) : (
        <Text style={styles.textStyle}>Open Now: Yes</Text>
      )}
      <Text style={styles.textStyle}>Address:</Text>
      {business.location.display_address ? (
        <FlatList
          data={business.location.display_address}
          keyExtractor={address => address}
          renderItem={({ item }) => {
            return <Text>{item}</Text>;
          }}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 25,
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#74C9B7',
    fontFamily: 'sans-serif-condensed',
    marginTop: 10
  },
  textStyle: {
    marginTop: 5
  },
  hoursStyle: {
    fontFamily: 'monospace',
    marginTop: 10
  },
  imageStyle: {
    height: 200,
    width: 300
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: '#74C9B7',
    padding: 10
  }
});

export default BusinessesShowScreen;
