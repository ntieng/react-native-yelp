import React from 'react';
import { Text, StyleSheet, View, Image } from 'react-native';
import { Rating } from 'react-native-ratings';

const BusinessesDetail = ({ business }) => {
  
  if (!business) {
    return null;
  }

  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{ uri: business.image_url }} />
      <Text style={styles.name}>{business.name}</Text>
      <View style={{ flexDirection: 'row' }}>
        <Rating
          imageSize={20}
          readonly
          startingValue={business.rating}
          style={styles.rating}
        />
        <Text style={{ marginLeft: 10 }}>{business.review_count} Reviews</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginLeft: 15
  },
  image: {
    width: 250,
    height: 120,
    borderRadius: 4,
    marginBottom: 5
  },
  name: {
    fontWeight: 'bold'
  },
  iconStyle: {
    alignSelf: 'center'
  }
});

export default BusinessesDetail;
